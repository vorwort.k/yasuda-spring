package com.yasuda.app;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@SpringBootApplication
public class AppApplication {

    // @RequestMapping("/")
    // public String home() {
    //     return "Hello Docker World!";
    // }

        @RequestMapping("/")
        public String home(Model model, @AuthenticationPrincipal OidcUser user) {
            model.addAttribute("username", user.getFullName());
            return "message_board";
    }



	public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
	}

}
