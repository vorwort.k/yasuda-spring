package com.yasuda.app;

import java.util.List;
import java.util.Map;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.ui.Model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

@RestController
public class DbConnect {

    @Autowired
	JdbcTemplate jdbcTemplate;

    @RequestMapping("/dbtest")
    public String dbtest(Model model) {
        List<Map<String,Object>> list;
        list = jdbcTemplate.queryForList("select * from users");
        return list.toString();
    }

    // @RequestMapping(path="/users/{name}", method=RequestMethod.GET)
    // public String read_users(@PathVariable String name) {
    //     List<Map<String,Object>> list;
    //     list = jdbcTemplate.queryForList("select * from users where name = ?", name);
    //     return list.toString();
    // }

    @RequestMapping(path="/insert_chat/{message}", method=RequestMethod.GET, produces="application/json")
    public List<Map<String,Object>> insert_chat(@PathVariable String message,@AuthenticationPrincipal OidcUser user) {
        LocalDateTime ldt = LocalDateTime.now().plusHours(9);
        jdbcTemplate.update("insert into chat values (?,?,?)",user.getFullName(), message,ldt);
        return jdbcTemplate.queryForList("select * from chat order by updated_time desc");
    }

    //Json形式で返すためにListで返す
    @RequestMapping(path="/select_chat", method=RequestMethod.GET, produces="application/json")
    public List<Map<String,Object>> select_chat() {
        // List<Map<String,Object>> list;
        // list = jdbcTemplate.queryForList("select * from chat");
        return jdbcTemplate.queryForList("select * from chat order by updated_time desc");
    }

    @RequestMapping("/auth")
    public String index(@AuthenticationPrincipal OidcUser user) {
        // logger.info(ArrayUtils.toString(user));
        return user.getFullName();
    }

}
